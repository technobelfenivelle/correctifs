import React, {  useState } from "react";
import Des from "./Dice";

export default function Plateau() {
  const [dice1, setDice1] = useState(Math.floor(Math.random() * 6) + 1);
  const [dice2, setDice2] = useState(Math.floor(Math.random() * 6) + 1);
  const [diceColor, setDiceColor] = useState("black");
  const [buttonColor, setButtonColor] = useState("btn btn-dark");

   

  const handleClick = () => {
    setDice1(Math.floor(Math.random() * 6) + 1);
    setDice2(Math.floor(Math.random() * 6) + 1);
    if (dice1 === dice2) {
        console.log("dice1 === dice2");
        setDiceColor("green");
        setButtonColor("btn btn-success");
      } else {
        setDiceColor("red");
        setButtonColor("btn btn-danger");
      }
  };

  return (
    <div>
      <div className="d-flex justify-content-around">
        <Des diceNumber={dice1} color={diceColor} />
        <Des diceNumber={dice2} color={diceColor} />
      </div>
      <div className="d-flex justify-content-center mt-5">
        <button className={buttonColor} onClick={handleClick}>
          Roll Dice !
        </button>
      </div>
    </div>
  );
}
