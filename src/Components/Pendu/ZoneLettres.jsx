import React from 'react'

const ZoneLettres = (props)=>
{
    const { Lettres, handleClick, LettreChoisies, isVisible} =  props;
 
    return (
        <div hidden={isVisible?"":"hidden"}>
          {
          Lettres.map((lettre) => (
            <button
              key={lettre}
              className={
                "aeiouyAEIOUY".indexOf(lettre) !== -1
                  ? "btn btn-outline-warning mt-3"
                  : "btn btn-outline-info mt-3"
              }
              disabled={LettreChoisies.indexOf(lettre)!==-1?"disabled":""}
              value={lettre}
              onClick={handleClick}
            >
              {lettre}
            </button>
          ))}
        </div>
      );
    
}

export default ZoneLettres;