import React, {useState,useEffect} from 'react'
import JsonWords from "../../datas/ListWord.json"; 
import Affichage from "./Affichage";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Alert from "react-bootstrap/Alert";
import Emotion from './Emotion';
import ZoneLettres from './ZoneLettres';
import Jumbotron from "react-bootstrap/Jumbotron";
const Pendu = (props)=>
{

    const [ motATrouver, setMotATrouver] = useState
    (
        JsonWords[Math.floor(Math.random() * JsonWords.length)].split("")
    );

    const [Alphabet, setAlphabet] = useState(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
      );
    const[CorrectLetters, setCorrectLetters]=useState([]);
    const[WrongLetters, setWrongLetters]=useState([]);
    const [visible,setVisible]=useState(true);
    const [Emoji,setEmoji]=useState("neutral");
    const [MaxFautes,setMaxFautes]= useState(motATrouver.length);
    const [NbFautes, setNbFautes]= useState(0);
    const [Choix, setChoix] = useState([]);
    const [MaLettre, setMaLettre]= useState("");
    const[AfficheMot,setAfficheMot]=useState("");
    const handleClick = (e)=>
    {
        console.log(e);
        let lettre = e.target.value;
        setMaLettre(lettre);
        setChoix([...Choix, lettre]);
         
    } 

    const MotCorrect=(accumulateur, currentValue)=>
    {
        return accumulateur && (CorrectLetters.indexOf(currentValue) >= 0)
    }

    useEffect(() =>
    {  
        if(MaLettre==="")
        {
            setEmoji("neutral");
        }
        else
        {
            if(motATrouver.includes(MaLettre))
            {
                console.log("good"); 
                setCorrectLetters([...CorrectLetters,MaLettre]); 
                setEmoji("good");
            }
            else
            {
                console.log("wrong");
                setEmoji("wrong");
                setNbFautes(NbFautes+1);
                setMaxFautes(MaxFautes-1);
                setWrongLetters([...WrongLetters,MaLettre]);
            }
           
            
        }       
        
    }, [MaLettre])   


    useEffect(()=>
    {
        if(motATrouver.reduce(MotCorrect, true))
        {
            setEmoji("success");
            setVisible(false);
        }
    },[CorrectLetters])

    useEffect(()=>{
        console.log(MaxFautes);
        if(MaxFautes===0)
        {
            setEmoji("lost");
            setVisible(false);
            setAfficheMot("Il fallait trouver " +motATrouver.join(''));
        }

    },[MaxFautes])



    return(
            <div>
                <h1>Le pendu</h1>
                <Container fluid>
                    <Row>
                        <Col sm={8}>
                           <Affichage Mot={motATrouver} LettreChoisies={Choix} />
                        </Col>
                        <Col sm={4}>
                            <Emotion emotion={Emoji}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={8}>
                        <Alert variant="success">
                            Lettres Correctes : {CorrectLetters.join(',')}
                            </Alert>
                            <br/>
                                <Alert variant="warning">
                                Lettres Fausses : {WrongLetters.join(',')}
                                </Alert>
                            <br/>
                            <Alert variant="danger">
                            Vies restantes : {MaxFautes}
                            </Alert>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <Jumbotron>{AfficheMot}</Jumbotron>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <ZoneLettres isVisible={visible} Lettres={Alphabet} handleClick={handleClick} LettreChoisies={Choix} />
                        </Col>
                    </Row>
                </Container>

            </div>
          );
}
export default Pendu;