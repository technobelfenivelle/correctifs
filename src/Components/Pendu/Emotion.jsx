import React, { useEffect, useState } from "react";
import emojis from "../../datas/emojis.json";

export default function Emotion(props) {
  const { emotion } = props;
  const [name, setName] = useState("");
  const [color, setColor] = useState("");
  const [path1, setPath1] = useState("");
  const [path2, setPath2] = useState("");

  useEffect(() => {
    emojis.forEach((emoji) => {
      if (emoji.id === emotion) {
        setName(emoji.name);
        setColor(emoji.color);
        setPath1(emoji.path1);
        setPath2(emoji.path2);
      }
    });
  }, [emotion]);
  return (
    <svg
      width="15em"
      height="15em"
      viewBox="0 0 16 16"
      className={name}
      fill={color}
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
      />
      <path fillRule="evenodd" d={path1} />
      <path d={path2} />
    </svg>
  );
}
