import React, { Fragment } from "react"; 
import style from "./Todo.module.css"; 
import { PRIORITY_TEXT } from '../../App';
const TaskCard =function(props)
{
    let statusClass=style.TaskStatusNormal;
    
    switch (props.Data.Priorite) {
        case '1':
            statusClass=style.TaskStatusUrgent;
            break;
            case '2':
            statusClass=style.TaskStatusNormal;
            break;
            case '3':
            statusClass=style.TaskStatusBas;
            break;
        default:
            statusClass=style.TaskStatusNormal;
            break;
    }
    
    return(
                <Fragment>
                    <p className={props.Data.Termine===1?style.TasktitreBarre:style.Tasktitre}>{props.Data.Nom}</p> 
                    
                    <p className={statusClass}>{PRIORITY_TEXT.get(props.Data.Priorite)}</p>
                    
                    <p className={props.Data.Termine===1?style.TaskDescriptionBarre:style.TaskDescription}>{props.Data.Description}</p>

                    <p className={props.Data.Termine===1?style.TaskFinished:style.TaskHidden}>Tache Terminée</p>
               </Fragment>  
        );
}

export default TaskCard;