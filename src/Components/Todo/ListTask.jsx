import React from "react"; 
import TaskCard from "./Task";
import shortid from 'shortid';
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"  
import Button from "react-bootstrap/Button"
const ListTask =function(props)
{ 
       

    return(
        <Container fluid>
            {props.Data.map((t)=>
                (
                    <Row key={shortid.generate()} style={{padding:"5px",border: "1px solid #000"}}>
                        <Col sm={8}>
                        
                                <TaskCard  Data={t} ></TaskCard>
                            
                        </Col> 
                        <Col sm={4} >
                            
                                <Button disabled={t.Termine===1?"disabled":""}
                                onClick={() => props.onRemove(t)}
                                style={{width:"100px"}} 
                                variant="danger">Supprimer</Button>
                                <br/><br/>
                                <Button disabled={t.Termine===1?"disabled":""}
                                onClick={() => props.onFinish(t)}
                                style={{width:"100px"}}  variant="primary">Terminer</Button>{' '}
                        </Col>
                    </Row>
                ))}  
        </Container> 
        );
}

export default ListTask;