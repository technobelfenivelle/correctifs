import React, {Component, Fragment} from 'react';
import FormTask from './FormTask';
import ListTask from './ListTask';

class Todo extends Component
{ 
    
    constructor(props)
    { 
        super(props);

        this.state=
        {
             tasks: []

            };
        
            this.addTask= this.addTask.bind(this);
            this.onFinish= this.onFinish.bind(this);
            this.onRemove= this.onRemove.bind(this);
    }

     


    addTask(task)
    { 
        this.setState(
        {
           tasks:[...this.state.tasks,task]
        });

        
         
    }

    onFinish(task)
    { 
        task.Termine=1; 
        //force le rafraichissement de l'UI
         this.setState(
             {
                tasks:[...this.state.tasks]
             });
    
    }

    onRemove(task)
    { 
         
        this.setState({tasks: this.state.tasks.filter(function(t) {          
            return t !== task
        })});
        
    }
    

    render()
    {
         
            return(
            
                <Fragment>
                    <div className="jumbotron">
                        <FormTask onSubmit={this.addTask} />
                    </div>
                
                    <div >
                        <h2>Liste des tâches</h2>                   
                        <ListTask Data={this.state.tasks} 
                        onFinish={this.onFinish} 
                        onRemove={this.onRemove}></ListTask>
                    </div>
                </Fragment>
            );
         
    }

}; 
export default Todo;