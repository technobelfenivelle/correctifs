import React, {Component} from 'react';
import { PRIORITY_NORMAL, PRIORITY_HIGH, PRIORITY_LOW } from '../../App';
class FormTask extends Component
{
    constructor(props)
    {
        super(props);
        this.state=
        {
            Nom:"",
            Description:"",
            Priorite:PRIORITY_NORMAL, 

        };
        this.handleInput= this.handleInput.bind(this);
        this.onSubmit= this.onSubmit.bind(this);
        
    }

    onSubmit(e)
    {
        e.preventDefault();
    
        
        
        if(this.props.onSubmit)
        {
            if(!this.state.Nom) {alert("Le nom est obligatoire!!"); return;}
            this.props.onSubmit(this.state);
        }
        this.setState(
            {
                Nom:"",
                Description:"",
                Priorite:PRIORITY_NORMAL,
                Termine:0 

        });
         

    }
    handleInput(e)
    { 
        
        const {name, type, checked, value} = e.target; 
        
        this.setState({[name]:type==='checkbox'?checked:value}); 
       
    } 
    render()
    {
      return(<div>
            <h1>Ajouter une nouvelle tâche</h1>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label htmlFor="Nom">Nom*</label>
                    <input className={"form-control"} type="text" name="Nom" value={this.state.Nom} onChange={this.handleInput}></input>
                </div>
                <div className={"form-group"}>
                    <label htmlFor="Description">Description</label>
                        <textarea  className={"form-control"} name="Description" value={this.state.Description} onChange={this.handleInput}></textarea>
                        <br/>
                        </div>
                <div className={"form-group"}>
                        
                        <label htmlFor="Priorite">Priorité</label>
                    <select  className={"form-control"} value={this.state.Priorite} name="Priorite" onChange={this.handleInput}>
                        <option value={PRIORITY_HIGH}>Urgent</option>
                        <option value={PRIORITY_NORMAL}>Normal</option>
                        <option value={PRIORITY_LOW}>Bas</option>
                    </select>
                    </div>
                <button className={"btn btn-primary"} >Add Task</button>
            </form>
        </div>
      );
    }
}

export default FormTask;