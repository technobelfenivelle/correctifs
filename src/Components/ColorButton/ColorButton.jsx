import {React, useState} from 'react'

const ColorButton= function(props)
{
    const tabColors = [
        "#eb4034",
        "#eb8634",
        "#ebe534",
        "#34eb71",
        "#34ebdc",
        "#3480eb",
        "#5f34eb",
        "#b134eb",
        "#eb3453",
      ];
  
    const[color,setColor]= useState("000");
    function changeMe()
    {
        let position = Math.floor(Math.random() * tabColors.length);

        setColor(tabColors[position]);
    }
    return (
    <button style={{backgroundColor : color}} onClick={changeMe}>{props.texte}</button>
    );
}

export default ColorButton;