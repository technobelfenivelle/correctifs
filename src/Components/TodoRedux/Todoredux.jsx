import React, {Fragment} from 'react'
import FormTask from "./FormTask"
import ListTask from "./ListTask"

const TodoRedux = ()=>
{
              return(
            
                <Fragment>
                    <div className="jumbotron">
                        <FormTask/>
                    </div>                
                    <div >
                        <h2>Liste des tâches</h2>                   
                        <ListTask></ListTask>
                    </div>
                </Fragment>
            ); 

}



export default TodoRedux;