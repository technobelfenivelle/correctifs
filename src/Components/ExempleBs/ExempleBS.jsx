import React from 'react'
import {connect} from 'react-redux'
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import shortid from 'shortid'

const ExempleBs = function(props)
{
   return (
            <Container fluid>
                {props.Data.map((t)=>
                    (
                        <Row key={shortid.generate()} style={{padding:"5px",border: "1px solid #000"}}>
                            <Col sm={8}>
                                {t.Nom}                    
                            </Col>  
                        </Row>
                    ))}  
            </Container> 
            );

}

const VachercherLEsdonnees = (state)=>
(
    {
        Data:state.TodoReducer.tasks
    }
);

//export default ExempleBs;
export default connect(VachercherLEsdonnees)(ExempleBs);