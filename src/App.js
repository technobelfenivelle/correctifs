import logo from './logo.svg';
import './App.css';
import Welcome from './Components/Exemple/welcome';
import Blablbla from './Components/blablbla/blablbla';
import ExempleBs from './Components/ExempleBs/ExempleBS';
import Todo from './Components/Todo/Todo';
import ColorButton from './Components/ColorButton/ColorButton';
import MagicDice from './Components/MagicDice/MagicDice';
import Pendu from './Components/Pendu/Pendu'
import TodoRedux from "./Components/TodoRedux/Todoredux"
import {BrowserRouter as Router ,
  Switch,
  Route,
  Link} from 'react-router-dom';
 



export const PRIORITY_HIGH = '1';
export const PRIORITY_NORMAL = '2';
export const PRIORITY_LOW = '3';
export const PRIORITY_TEXT = new Map();
PRIORITY_TEXT.set(PRIORITY_LOW, 'Bas');
PRIORITY_TEXT.set(PRIORITY_NORMAL, 'Normal');
PRIORITY_TEXT.set(PRIORITY_HIGH, 'Urgent');

function App() {
  return (
    <div className="App">
      <header className="App-header"> 
        <img src={logo} className="App-logo" alt="logo" />         
      </header>
      
      <Router>
      <div>
        {/*Menu*/}
        <nav>
          <ul>
            <li> 
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/boostrap">BootStap 1</Link>
            </li>
            <li>
              <Link to="/ExempleBS">BootStap 2</Link>
            </li>
            <li>
              <Link to="/Todo">Todo</Link>
            </li>
            <li>
              <Link to="/TodoRedux">Todo avec Redux</Link>
            </li>
            
            <li>
              <Link to="/Magic">Deux dés</Link>
            </li>
            <li>
              <Link to="/Rainbow">Un boutton arc-en-ciel</Link>
            </li>
            
            <li>
              <Link to="/Pendu">Une corde ?</Link>
            </li>
          </ul>
        </nav>

        {/*Zones de navigation*/}
        <Switch>
          <Route path="/boostrap">
            <Blablbla />
          </Route>
          <Route path="/ExempleBS">
            <ExempleBs/>
          </Route>
          <Route path="/Todo">
            <Todo/>
          </Route> 
          <Route path="/TodoRedux">             
                <TodoRedux/> 
          </Route>         
          <Route path="/Rainbow">
            <ColorButton texte="Hello" />
          </Route>
          <Route path="/Magic">
            <MagicDice />
          </Route>
          <Route path="/Pendu">
            <Pendu/>
          </Route>          
          <Route path="/">
          <Welcome name="Yoda" lang="React"></Welcome>
          </Route>

        </Switch>
      </div>
    </Router>

      
    </div>
  );
}

export default App;
