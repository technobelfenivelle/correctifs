//Import des constantes pour le type des actions
import {TASK_ADDED, TASK_FINISHED, TASK_DELETED} from '../actions/actions-TodoRedux'

//Valeur initiale du Global state
const initialState=
{
    tasks:[]
}

//Fonction Reducer
const TodoReducer =(state=initialState,action)=>
{
    switch(action.type)
    {
        //Pour ajouter une tâche
        case TASK_ADDED : return addTask(action.taskToAdd, state); 
        //Pour supprimer une tâche
        case TASK_DELETED: return onRemove(action.taskToDelete,state);
        //Pour terminer une tâche
        case TASK_FINISHED: return onFinish(action.taskToFinish,state);
        default : 
            return state;
    }
}

function addTask(task,state)
    {  
        state=
        {
           ...state, 
           tasks:[...state.tasks,task]
        };
        return state;
    }

function  onFinish(task, state)
    {   
        //a retravailler car change l'ordre des tâches
        task.Termine=1;  
        state=
             {
                 ...state,
                tasks:[...state.tasks.filter(function(t) 
                                            {          
                                                return t !== task
                                            }
                                        ), task]
                                     
             };
        return state; 
    
    }

 function  onRemove(task,state)
    { 
         state=
             {
                 ...state,
                tasks:state.tasks.filter(function(t) 
                                            {          
                                                return t !== task
                                            }
                                        )
                                     
             };
        return state;
    }

export default TodoReducer

 