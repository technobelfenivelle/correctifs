//Déclarer les constantes qui serviront d'étiquette pour mes actions

export const TASK_ADDED ="TASK_ADDED";
export const TASK_FINISHED ="TASK_FINISHED";
export const TASK_DELETED ="TASK_DELETED";

//Décrire les fonctions d'action (type + paramètres)

export const taskAdd = (taskToAdd)=>
(
    {
        type:TASK_ADDED, //OBLIGATOIRE!!!!!
        taskToAdd : taskToAdd //paramètre définissant la tâche à ajouter
    }
);
export const taskFinish = (taskToFinish)=>
(
    {
        type:TASK_FINISHED, //OBLIGATOIRE!!!!!
        taskToFinish 
    }
);
export const taskDelete = (taskToDelete)=>
(
    {
        type:TASK_DELETED, //OBLIGATOIRE!!!!!
        taskToDelete 
    }
);